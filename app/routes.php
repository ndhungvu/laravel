<?php

Route::get('/','HomeController@showWelcome');

/*----------------ADMIN-------------------*/
Route::group(array('before' => 'auth'), function() {
    Route::get('/admin',array('as'=>'admin.dashboard', 'uses'=> 'AdminController@index'));
    /*Upload image*/
    Route::post('/admin/uploadImage', 'AdminController@uploadImage');

    Route::group(array('prefix' => 'admin', 'namespace' => 'Admin'), function () {
        /*---Groups---*/
        Route::get('/groups',array('as'=>'admin.groups', 'uses'=>'GroupController@index'));
        Route::get('/group/add',array('as'=>'admin.group.add','uses'=>'GroupController@getCreate'));
        Route::post('/group/add', 'GroupController@postCreate');
        Route::get('/group/detail/{id}', array('as'=>'admin.group.detail', 'uses'=>'GroupController@getDetail'));
        Route::get('/group/edit/{id}', array('as'=>'admin.group.edit', 'uses'=>'GroupController@getEdit'));
        Route::post('/group/edit/{id}', 'GroupController@postEdit');
        Route::post('/group/delete/{id}', array('as'=>'admin.group.delete', 'uses'=>'GroupController@postDelete'));
        Route::post('/group/deleteAll', array('as'=>'admin.group.deleteAll', 'uses'=>'GroupController@postDeleteAll'));

        /*---Users---*/
        Route::get('/users',array('as'=>'admin.users', 'uses'=>'UserController@index'));
        Route::get('/user/add',array('as'=>'admin.user.add','uses'=>'UserController@getCreate'));
        Route::post('/user/add', 'UserController@postCreate');
        Route::get('/user/detail/{id}', array('as'=>'admin.user.detail', 'uses'=>'UserController@getDetail'));
        Route::get('/user/edit/{id}', array('as'=>'admin.user.edit', 'uses'=>'UserController@getEdit'));
        Route::post('/user/edit/{id}', 'UserController@postEdit');
        Route::post('/user/delete/{id}', array('as'=>'admin.user.delete', 'uses'=>'UserController@postDelete'));
        Route::post('/user/deleteAll', array('as'=>'admin.user.deleteAll', 'uses'=>'UserController@postDeleteAll'));

        /*---Categories---*/
        Route::get('/categories',array('as'=>'admin.categories', 'uses'=>'CategoryController@index'));
        Route::get('/category/add',array('as'=>'admin.category.add','uses'=>'CategoryController@getCreate'));
        Route::post('/category/add', 'CategoryController@postCreate');
        Route::get('/category/detail/{id}', array('as'=>'admin.category.detail', 'uses'=>'CategoryController@getDetail'));
        Route::get('/category/edit/{id}', array('as'=>'admin.category.edit', 'uses'=>'CategoryController@getEdit'));
        Route::post('/category/edit/{id}', 'CategoryController@postEdit');
        Route::post('/category/delete/{id}', array('as'=>'admin.category.delete', 'uses'=>'CategoryController@postDelete'));
        Route::post('/category/deleteAll', array('as'=>'admin.category.deleteAll', 'uses'=>'CategoryController@postDeleteAll'));
        Route::post('/category/changeStatus/{id}/{status}', array('as'=>'admin.category.changeStatus', 'uses'=>'CategoryController@postChangeStatus'));
        Route::post('/category/positionUp/{id}', array('as'=>'admin.category.positionUp', 'uses'=>'CategoryController@postPositionUp'));
        Route::post('/category/positionDown/{id}', array('as'=>'admin.category.positionDown', 'uses'=>'CategoryController@postPositionDown'));
    });
});

/*USERS*/
Route::group(array('namespace' => 'Admin'), function () {
	Route::get('/login',array('as'=>'admin.user.login', 'uses'=>'UserController@getLogin'));
	Route::post('/login',array('as'=>'admin.user.login', 'uses'=>'UserController@postLogin'));
	Route::get('/logout', array('as'=>'admin.user.logout', 'uses'=>'UserController@getLogout'));
});
