<?php
/*This is function create alias of title*/
function alias($model, $title) {
	$alias = Str::slug($title);
    $aliasCount = count( $model->whereRaw("title REGEXP '^{$alias}(-[0-9]*)?$'")->get() );
    return ($aliasCount > 0) ? "{$alias}-{$aliasCount}" : $alias;
}
?>