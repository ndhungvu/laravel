<?php
namespace Admin;
use View, Input, Validator, Redirect, Auth, Hash, Response;
class UserController extends \AdminController {
    protected $layout = 'admin.layouts.login';

    /**
     * This is function used call login form
     */
	public function getLogin() {
		$this->layout->title = "Login";
		$this->layout->main = View::make('admin/user/login');
	}

	/**
	 * This is function used login system
	 */
	public function postLogin() {

		$input = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
		);

		$rules = array(
	       'email' => 'required|email',
	       'password' => 'required'
		);

		$valid = Validator::make($input, $rules);
		if($valid->fails()) {
			return Redirect::back()->withErrors($valid);
		}

		if (Auth::attempt(array('email' => $input['email'], 'password' => $input['password'], 'status' => 1, 'group_id'=> 1))){
			$user = Auth::user();
			return Redirect::route('admin.dashboard')->with('flashSuccess', 'You are now logged in');
		}
		return Redirect::back()->with('flashError', 'Your email/password was incorrect');
	}

	/**
	 * This is function used logout system
	 */
	public function getLogout() {
		Auth::logout();
		Return Redirect::route('admin.user.login');
	}

    public function index() {
    	$title = 'Users';
    	$users = \User::orderBy('created_at', 'DESC')->paginate(10);
    	return View::make('/admin/user/index', compact('title', 'users'));
    }

    public function getDetail($id) {
    	$title = 'Detail';
    	$user = \User::where('id',$id)->first();
    	if(!empty($user)) {
    		return View::make('/admin/user/detail',compact('title', 'user'));
    	}
    }

    public function getCreate() {
    	$title = 'Users - Create';
    	/*Get groups*/
    	$groups = \Group::where('status', \Group::ACTIVE)->get();
    	return View::make('/admin/user/create', compact('title', 'groups'));
    }

    public function postCreate() {
    	$input = array(
    	   'username' => Input::get('username'),
    	   'password' => Input::get('password'),
    	   'password_confirmation' => Input::get('password_confirmation'),
    	   'email' => Input::get('email'),
    	   'first_name' => Input::get('first_name'),
    	   'last_name' => Input::get('last_name'),
    	   'phone' => Input::get('phone'),
    	   'fax' => Input::get('fax'),
    	);

    	$valid = array(
    	   'username' => 'required|alpha_num|min:6',
    	   'password' => 'required|min:6|confirmed',
    	   'password_confirmation' =>'required|min:6',
    	   'email' => 'required|email',
    	   'first_name' => 'required|alpha',
    	   'last_name' => 'required|alpha',
    	   'phone' => 'numeric',
    	   'fax' => 'numeric',
    	);

    	$v = Validator::make($input, $valid);
    	if($v->fails()) {
    		return Redirect::back()->withInput()->withErrors($v);
    	}

        $user = new \User();
        $image_tmp = Input::get('image_tmp');
        if(!empty($image_tmp)) {
            $path_image_tmp = base_path().$image_tmp;
            
            $imageName = md5(time());
            $size = getimagesize(base_path().$image_tmp);
            $ext = image_type_to_extension($size[2]);

            $path_image_new = base_path().'/uploads/avatar/'.$imageName.$ext;
            $content = file_get_contents($path_image_tmp);            
            if(file_put_contents($path_image_new, $content)) {
                /*Delete image tmp*/ 
                \File::delete($path_image_tmp);
                $user->avatar = 'uploads/avatar/'.$imageName.$ext;
            }
        }else {
            if(Input::get('genre') == 1)
               $user->avatar = 'uploads/avatar/no-avatar-male.jpg';
            else
               $user->avatar = 'uploads/avatar/no-avatar-woman.jpg';
        }

    	
    	$user->username = Input::get('username');
    	$user->password = Hash::make(Input::get('password'));
    	$user->email = Input::get('email');
    	$user->first_name = Input::get('first_name');
    	$user->last_name = Input::get('last_name');
    	
    	$user->birthday = date('Y-m-d H:i:s', strtotime(Input::get('birthday')));
    	$user->genre = Input::get('genre');
    	$user->phone = Input::get('phone');
    	$user->address = Input::get('address');
    	$user->fax = Input::get('fax');
    	$user->group_id = Input::get('group_id');
    	$user->status = Input::get('status');
    	$user->created_at = date('Y-m-d H:i:s', time());
    	$user->updated_at = date('Y-m-d H:i:s', time());
    	if($user->save()) {
    		return Redirect::route('admin.user.detail', $user->id)->with('flashSuccess', 'User is created');
    	}else {
    		return Redirect::back()->with('flashError', 'User created fail');
    	}
    }

        private function dirName(){
        $str = "coupon-" . date('Ymd'). '-' . str_random(9);
        if (file_exists(public_path() . '/uploads/' . $str)) {
            $str = $this->dirName();
        }
        return $str;
    }
   
    public function getEdit($id) {
    	$title = "Users - Edit";
    	$user = \User::where('id', $id)->first();
    	if(!empty($user)) {
    		/*Get groups*/
            $groups = \Group::where('status', \Group::ACTIVE)->get();
    		return View::make('/admin/user/edit',compact('title', 'user', 'groups'));
    	}else {
    		
    	}
    }

    public function postEdit($id) {
        $input = array(
           'username' => Input::get('username'),
           'email' => Input::get('email'),
           'first_name' => Input::get('first_name'),
           'last_name' => Input::get('last_name'),
           'phone' => Input::get('phone'),
           'fax' => Input::get('fax'),
        );

        $valid = array(
           'username' => 'required|alpha_num|min:6',
           'email' => 'required|email',
           'first_name' => 'required|alpha',
           'last_name' => 'required|alpha',
           'phone' => 'numeric',
           'fax' => 'numeric',
        );

        $v = Validator::make($input, $valid);
        if($v->fails()) {
            return Redirect::back()->withInput()->withErrors($v);
        }
        $user = \User::where('id', $id)->first();

        $image_tmp = Input::get('image_tmp');
        if(!empty($image_tmp)) {
            $path_image_tmp = base_path().$image_tmp;
            
            $imageName = md5(time());
            $size = getimagesize(base_path().$image_tmp);
            $ext = image_type_to_extension($size[2]);

            $path_image_new = base_path().'/uploads/avatar/'.$imageName.$ext;
            $content = file_get_contents($path_image_tmp);            
            if(file_put_contents($path_image_new, $content)) {
                /*Delete image tmp*/ 
                \File::delete($path_image_tmp);               
                /*Delete image old*/
                //\File::delete($user->avatar);
                /*Save avatar new*/
                $user->avatar = 'uploads/avatar/'.$imageName.$ext;
            }
        }

        $user->username = Input::get('username');
        $user->email = Input::get('email');
        $user->first_name = Input::get('first_name');
        $user->last_name = Input::get('last_name');
        $user->birthday = date('Y-m-d H:i:s', strtotime(Input::get('birthday')));
        $user->genre = Input::get('genre');
        $user->phone = Input::get('phone');
        $user->address = Input::get('address');
        $user->fax = Input::get('fax');
        $user->group_id = Input::get('group_id');
        $user->status = Input::get('status');
        $user->updated_at = date('Y-m-d H:i:s', time());
        if($user->save()) {
            return Redirect::route('admin.user.detail', $user->id)->with('flashSuccess', Lang::get('messages.updated_success'));
        }else {
            return Redirect::back()->with('flashError', 'User updated fail');
        }
    }

    /*Delete user*/
    public function postDelete($id) {
    	$user = \User::where('id', $id)->first();
    	if(!empty($user)) {
    		if($user->delete()) {
                return Response::json(array('error'=>false, 'message'=> 'User is deleted'));
            }
            return Response::json(array('error'=>true, 'message'=> 'User deleted fail'));
    	}else {

    	}
    }

    /*Delete all user*/
    public function postDeleteAll() {
        $checkboxes = $_POST['id'];
        $ok = true;
        foreach ($checkboxes as $id) {
            $user = \User::where('id', $id)->first();
            if(!$user->delete()) {
                $ok = false;
                break;
            }
        }

        if($ok) {
            return Response::json(array('error'=>false, 'message'=> 'Users is deleted'));
        }else {
           return Response::json(array('error'=>true, 'message'=> 'Users delete fail'));
        }
    }
}