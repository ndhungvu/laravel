<?php

class AdminController extends BaseController {

    protected $layout = 'admin.layouts.dashboard';

	public function index() {
		$this->layout->title = "Dashboard";
        $this->layout->content = View::make('admin/user/login');
	}

	public function uploadImage() {     
        $file = Input::file('image');
        if (!empty($file)) {
            $fileSize = @$file->getSize();
            if ( ! $fileSize || ! $file->getMimeType()) {
            	return Response::json(array('status'=>false, 'message'=> 'Upload image is fail'));
            }
            $imageName = md5(time());
            $mimetype = preg_replace('/image\//', '', $file->getMimeType());
            $pathUpload = 'uploads/tmp/';
            Input::file('image')->move($pathUpload, $imageName.'.'.$mimetype);
            $strImage = '/'.$pathUpload.$imageName.'.'.$mimetype;
            return Response::json(array('status'=>true, 'message'=> 'Upload image is successful', 'data'=> $strImage));
        }
        return Response::json(array('status'=>false, 'message'=> 'Upload image is fail'));
    }    
}
