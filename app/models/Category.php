<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Category extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';
    const ACTIVE = 1;
    const UNACTIVE = 0;
	
	public function getPositionByParrentID($parentID) {
		$position = DB::table('categories')->where('parent_id', $parentID)->max('position');
		return $position == 0 ? 1 : $position + 1;
	}
	public static function getParrent($id) {
		$category =  DB::table('categories')->where('id', $id)->first();
		return $category;
	}
}
