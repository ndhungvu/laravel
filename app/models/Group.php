<?php
use Cviebrock\EloquentTypecast\EloquentTypecastTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class Group extends Eloquent {
	protected $table = "groups";
    const ACTIVE = 1;
    const UNACTIVE = 0;
	public function users() {
		return $this->hasMany('users', 'group_id');
	}
}