@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">User</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <p><h3 class="box-title">List</h3></p>
                    <a class="btn btn-primary btn-sm" style="margin-right: 3px;" href="{{URL::route('admin.group.add')}}" data-toggle="tooltip" data-placement="top" title="Add"><i class="fa fa-fw fa-plus"></i></a>
                    @if (count($groups) > 0)
                    <a class="btn btn-danger btn-sm jsDeleteAll" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Delete All"><i class="fa fa-fw fa-trash"></i></a>
                    @endif
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                            @if (!empty($groups))
                                {{Form::open(array('url'=>'admin/group/deleteAll', 'id'=>'frmDeleteAll', 'class'=>'form-horizontal'))}}
                                <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                            <th><input type="checkbox" class="jsCheckboxAll"/></th>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Created date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($groups as $index => $group)
                                        <tr role="row" class="odd">
                                            <td><input type="checkbox" name="id[]" value="{{$group->id}}" class="jsCheckbox"/></td>
                                            <td class="sorting_1">{{$index + 1}}</td>
                                            <td>{{$group->name}}</td>
                                            <td>{{$group->description}}</td>
                                            <td>{{$group->status == 1 ? 'Active' : 'UnActive'}}</td>
                                            <td>{{$group->created_at}}</td>
                                            <td>
	                                            <a class="btn btn-success btn-xs" href="{{URL::route('admin.group.detail', $group->id)}}" data-toggle="tooltip" data-placement="top" title="View" role="button"><i class="fa fa-fw fa-eye"></i></a>
	                                            <a class="btn btn-primary btn-xs" href="{{URL::route('admin.group.edit', $group->id)}}" data-toggle="tooltip" data-placement="top" title="Edit" role="button"><i class="fa fa-fw fa-edit"></i></a>
	                                            <a href="javascript:;" class="btn btn-danger btn-xs jsDelete" attr_href="{{URL::route('admin.group.delete', $group->id)}}" data-toggle="tooltip" data-placement="top" title="Delete" role="button"><i class="fa fa-fw fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{Form::close()}}
                                @if (count($groups) == 0)<h3 class="box-title"><small>No result</small></h3> @endif
                                {{$groups->links()}}
                            @endif
                           </div>
                       </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
@stop