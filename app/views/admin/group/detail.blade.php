@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{URL::route('admin.groups')}}">Groups</a></li>
            <li class="active">{{$title}}</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="col-xs-6">
                        <h3 class="box-title">{{$title}}</h3>
                    </div>
                    <div class="col-xs-6">
                        <a class="btn btn-primary btn-sm" style="float: right;" href="{{URL::route('admin.group.edit', $group->id)}}">Edit</a>
                    </div>
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="group_detail" class="table table-bordered" role="grid">
                                    <tbody>
                                        <tr>
                                            <td class="col-sm-3">ID</td>
                                            <td>{{$group->id}}</td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td>{{$group->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Description</td>
                                            <td>{{$group->description}}</td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>{{$group->status == 1 ? 'Active' : 'UnActive'}}</td>
                                        </tr>
                                        <tr>
                                            <td>Created date</td>
                                            <td>{{$group->created_at}}</td>
                                        </tr>
                                        <tr>
                                            <td>Created update</td>
                                            <td>{{$group->updated_at}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop