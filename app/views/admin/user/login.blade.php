<div class="login-box">
    <div class="login-box-body">        
        <img src="{{asset('assets/admin/dist/img/login.jpg')}}" alt="Login" style="margin-bottom: 15px; width : 100%"/>
        @include('admin/layouts.login_flash')
        {{Form::open()}}
            <div class="form-group has-feedback">
                {{Form::email('email', '', array('class'=>'form-control', 'placeholder'=>'Email'))}}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                {{Form::password('password', array('class' => 'form-control', 'placeholder'=>'Password'))}}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            {{Form::checkbox('active', 'value', false)}} Remember Me
                        </label>
                    </div>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    {{Form::submit('Sign In', array('class'=>'btn btn-primary btn-block btn-flat'))}}
                </div>
          </div>
        {{Form::close()}}
        <a href="javascript:;">I forgot my password</a><br>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->