<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="treeview @if(str_is('admin/user*', Request::path())) active @endif">
                <a href="javascript:;">
                    <i class="fa fa-pie-chart"></i>
                    <span>Users</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(Route::current()->getName() == 'admin.user.add') active @endif"><a href="{{URL::route('admin.user.add')}}"><i class="fa fa-circle-o"></i> Add</a></li>
                    <li class="@if(Route::current()->getName() == 'admin.users') active @endif"><a href="{{URL::route('admin.users')}}"><i class="fa fa-circle-o"></i> Lists</a></li>
                </ul>
            </li>
            <li class="treeview @if(str_is('admin/group*', Request::path())) active @endif">
                <a href="javascript:;">
                    <i class="fa fa-pie-chart"></i>
                    <span>Groups</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(Route::current()->getName() == 'admin.group.add') active @endif"><a href="{{URL::route('admin.group.add')}}"><i class="fa fa-circle-o"></i> Add</a></li>
                    <li class="@if(Route::current()->getName() == 'admin.groups') active @endif"><a href="{{URL::route('admin.groups')}}"><i class="fa fa-circle-o"></i> Lists</a></li>
                </ul>
            </li>
            <li class="treeview @if(str_is('admin/group*', Request::path())) active @endif">
                <a href="javascript:;">
                    <i class="fa fa-pie-chart"></i>
                    <span>Categories</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(Route::current()->getName() == 'admin.category.add') active @endif"><a href="{{URL::route('admin.category.add')}}"><i class="fa fa-circle-o"></i> Add</a></li>
                    <li class="@if(Route::current()->getName() == 'admin.categories') active @endif"><a href="{{URL::route('admin.categories')}}"><i class="fa fa-circle-o"></i> Lists</a></li>
                </ul>
            </li>
            
          </ul>
        </section>
        <!-- /.sidebar -->
</aside>