@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::route('admin.categories')}}">Categories</a></li>
            <li class="active">Detail</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="col-xs-6">
                        <h3 class="box-title">Detail</h3>
                    </div>
                    <div class="col-xs-6">
                        <a class="btn btn-primary btn-sm" style="float: right;" href="{{URL::route('admin.category.edit', $category->id)}}">Edit</a>
                    </div>
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="category_detail" class="table table-bordered" role="grid">
                                    <tbody>
                                        <tr>
                                            <td class="col-sm-3">ID</td>
                                            <td>{{$category->id}}</td>
                                        </tr>
                                        <tr>
                                            <td>Title</td>
                                            <td>{{$category->title}}</td>
                                        </tr>
                                        <tr>
                                            <td>Alias</td>
                                            <td>{{$category->alias}}</td>
                                        </tr>
                                        <tr>
                                            <td>Description</td>
                                            <td>{{$category->description}}</td>
                                        </tr>
                                        <tr>
                                            <td>Position</td>
                                            <td>{{$category->position}}</td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>{{$category->status == 1 ? 'Active' : 'UnActive'}}</td>
                                        </tr>
                                        <tr>
                                            <td>Position</td>
                                            <td>{{$category->position}}</td>
                                        </tr>
                                        <tr>
                                            <td>Created date</td>
                                            <td>{{$category->created_at}}</td>
                                        </tr>
                                        <tr>
                                            <td>Created update</td>
                                            <td>{{$category->updated_at}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop