@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::route('admin.categories')}}">Categories</a></li>
            <li class="active">Create</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                {{Form::open(array('id'=>'frmCategoryAdd', 'class'=>'form-horizontal', 'files'=> TRUE))}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Title<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{Form::text('title', '', array('class'=>'form-control'))}}
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label for="password_confirmation" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('description', null, ['size' => '30x5', 'class'=>'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="password_confirmation" class="col-sm-2 control-label">Parent</label>
                            <div class="col-sm-5">
                                <select name="parent_id" class="form-control">
                                    <option value="0">--- Select ---</option>
                                    @if (!empty($parents))
    	                                @foreach ($parents as $parent)
    	                                <option value="{{$parent->id}}">{{$parent->title}}</option>
    	                                @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="checkbox">
                            {{Form::label('','',array('class'=> 'col-sm-2 control-label'))}}
                            <label>
                                {{Form::checkbox('status', null , false)}} Active
                            </label>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a class="btn btn-primary" href="{{URL::route('admin.categories')}}">Back</a>
                        <button type="submit" class="btn btn-primary">OK</button>
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</select>
@stop