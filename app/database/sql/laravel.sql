/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : laravel

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2015-06-08 16:46:09
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `description` text,
  `status` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'admin', 'Admin', '1', '2015-06-08 16:39:23', '2015-06-08 16:39:26');
INSERT INTO `groups` VALUES ('2', 'User', 'User', '1', '2015-06-08 09:45:38', '2015-06-08 09:45:38');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `first_name` varchar(256) DEFAULT NULL,
  `last_name` varchar(256) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `phone` varchar(256) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `genre` int(1) DEFAULT '0',
  `avatar` varchar(256) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `salf` varchar(256) DEFAULT NULL,
  `is_login` int(1) DEFAULT '0',
  `remember_token` varchar(256) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_group` (`group_id`),
  CONSTRAINT `fk_group` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'vieteufood', '$2y$10$aPx8C5NGf3a81yb0PQVj3uJ7cxtzqfGbm/APlcChkRgp1uzr4pJqS', 'admin@gmail.com', 'Vu', 'Nguyen', '1989-05-04 00:00:00', null, '01697916836', '01655969264', '1', 'uploads/avatar/b7789b9c4473afe449fb53bc332bd7c5.jpeg', '1', null, '0', null, '2015-06-08 16:41:40', '2015-06-08 09:44:32');
